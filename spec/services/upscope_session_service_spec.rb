require 'rails_helper'

describe UpscopeSessionService do
  describe '#perform' do
    context 'user not found' do
      let(:response) { double(:response, body: { visitors: [] }.to_json) }
      let(:params) do
        {
          lookup_code: '1234',
          agent_name: 'Foo',
          agent_email: 'foo@acme.com'
        }
      end

      it 'returns a message' do
        expect(HTTParty).to receive(:get).with(
          described_class::VISITOR_URL + "?search=1234",
          headers: {
            'X-Api-Key' => Rails.application.credentials.dig(:upscope, :secret_api_key)
          }
        ).and_return(response)

        result = described_class.new(params).perform

        expect(result.message).to eq 'User Not Found'
      end
    end

    context 'user found' do
      let(:get_response) { double(:get_response, body: { visitors: [{short_id: 'ABC_123'}] }.to_json) }
      let(:post_response) { double(:post_respons, ok?: true, body: { watch_url: 'https://upscope.io?12345' }.to_json) }

      let(:params) do
        {
          lookup_code: '2143',
          agent_name: 'Foo',
          agent_email: 'foo@acme.com'
        }
      end

      it 'returns the watch url' do
        expect(HTTParty).to receive(:get).with(
          described_class::VISITOR_URL + "?search=2143",
          headers: {
            'X-Api-Key' => Rails.application.credentials.dig(:upscope, :secret_api_key)
          }
        ).and_return(get_response)

        expect(HTTParty).to receive(:post).with(
          'https://api.upscope.io/v1.1/visitors/ABC_123/watch_url.json',
          headers: {
            'X-Api-Key' => Rails.application.credentials.dig(:upscope, :secret_api_key),
            'Content-Type' => Mime[:json].to_s,
            'Accept' => Mime[:json].to_s
          },
          body: {
            branding: {
              retry_url: 'http://localhost:3000/cobrowse',
              on_end_url: 'http://localhost:3000'
            },
            agent: {
              id: 'foo@acme.com',
              name: 'Foo'
            }
          }.to_json
        ).and_return(post_response)

        result = described_class.new(params).perform

        expect(result.watch_url).to eq 'https://upscope.io?12345'
      end
    end
  end
end
