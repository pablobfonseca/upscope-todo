# frozen_string_literal: true

require 'rails_helper'

feature 'As an user I want to' do
  scenario 'create a new item' do
    visit root_path

    expect(page).to have_content 'New Item'

    click_on 'New Item'

    fill_in 'Description', with: 'Foo and Bar'
    fill_in 'Due Date', with: '01/01/2020'

    click_on 'Create Item'

    expect(page).to have_content 'New Item was added successfully'
  end
end
