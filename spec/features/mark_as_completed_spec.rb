require 'rails_helper'

feature 'As an User I want to' do
  background do
    @item = Item.create(description: 'Foo and bar', due_date: '02/02/2020'.to_date)
  end

  scenario 'mark item as completed' do
    visit root_path

    expect do
      within ".item-#{@item.id}" do
        click_on 'Complete'
      end
    end.to change { @item.reload.completed }
  end
end
