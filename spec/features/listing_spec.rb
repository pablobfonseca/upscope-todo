# frozen_string_literal: true

require 'rails_helper'

feature 'When I access the root path' do
  context 'and the item is completed' do
    background do
      @item = Item.create(description: 'Foo', due_date: '02/02/2020'.to_date, completed: true)
    end

    scenario 'I should not see the complete button' do
      visit root_path

      within ".item-#{@item.id}" do
        expect(page).not_to have_content('Complete')
      end
    end

    scenario 'I should not see the delete' do
      visit root_path

      within ".item-#{@item.id}" do
        expect(page).not_to have_content('Delete')
      end
    end
  end

  context 'and the item is incompleted' do
    background do
      @item = Item.create(description: 'Foo', due_date: '02/02/2020'.to_date)
    end

    scenario 'I should see the complete button' do
      visit root_path

      within ".item-#{@item.id}" do
        expect(page).to have_content('Complete')
      end
    end

    scenario 'I should see the delete button' do
      visit root_path

      within ".item-#{@item.id}" do
        expect(page).to have_content('Delete')
      end
    end
  end
end
