require 'rails_helper'

feature 'As an User I want to' do
  scenario 'delete an item' do
    item = Item.create(description: 'foo', due_date: '01/01/2020'.to_date)

    visit root_path

    expect do
      within ".item-#{item.id}" do
        click_on 'Delete'
      end
    end.to change(Item, :count).to(0)
  end
end
