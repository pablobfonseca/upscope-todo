# frozen_string_literal: true

require 'rails_helper'

feature 'As an User I want to' do
  scenario 'edit an item' do
    item = Item.create(description: 'foo', due_date: '02/02/2020'.to_date)

    visit root_path

    within ".item-#{item.id}" do
      click_on 'Edit'
    end

    fill_in 'Description', with: 'Bar'
    click_on 'Update Item'

    expect(item.reload.description).to eq 'Bar'
  end
end
