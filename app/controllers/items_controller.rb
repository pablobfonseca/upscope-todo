# frozen_string_literal: true

class ItemsController < ApplicationController
  before_action :set_item, only: [:edit, :update, :destroy, :complete]

  def index
    @items = Item.all
  end

  def new
    @item = Item.new
  end

  def edit; end

  def update
    @item.update(item_params)

    redirect_to root_path, notice: 'Item has been updated'
  end

  def create
    @item = Item.new(item_params)

    @item.save

    redirect_to root_path, notice: 'New Item was added successfully'
  end

  def destroy
    @item.destroy

    redirect_to root_path, notice: 'Item has been deleted'
  end

  def complete
    @item.update(completed: true)

    redirect_to root_path, notice: 'Item has been completed'
  end

  private

  def set_item
    @item = Item.find params[:id]
  end

  def item_params
    params.require(:item).permit(:description, :due_date)
  end
end
