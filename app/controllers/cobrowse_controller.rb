class CobrowseController < ApplicationController
  before_action :authenticate

  def new
    @agent_name = @username.split('@').first.capitalize
  end

  def create
    result = UpscopeSessionService.new(upscope_params).perform

    render json: { watch_url: result.watch_url, message: result.message }
  end

  private

  def upscope_params
    params.require(:upscope_session).permit(:lookup_code, :agent_name, :agent_email)
  end

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      if username.match(/[a-zA-Z0-9]+@acme.com/) && password == Rails.application.credentials[:acme_password]
        @username = username
      end
    end
  end
end
