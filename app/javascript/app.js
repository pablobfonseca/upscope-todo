$(() => {
  const link = document.querySelector('.upscope-link')

  link.addEventListener('click', (e) => {
    e.preventDefault()
    Upscope('getLookupCode', (code) => {
      alert(`Code: ${code}`)
    })
  })
})
