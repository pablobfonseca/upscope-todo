$(() => {
  const input = document.getElementById('upscope_session_lookup_code')
  const error = document.querySelector('.invalid-feedback')
  error.textContent = ''
  error.style.display = 'none'

  input.addEventListener('keyup', (e) => {
    let code = input.value

    if (code.length === 4) {
      e.stopPropagation()
      requestSession(code)
    }
  })

  function requestSession(lookup_code) {
    const csrfToken = document.querySelector('meta[name="csrf-token"]').attributes.content.value
    const agentName = document.getElementById('upscope_session_agent_name').value
    const agentEmail = document.getElementById('upscope_session_agent_email').value

    fetch('/cobrowse', {
      method: 'POST',
      mode: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "X-CSRF-Token": csrfToken

      },
      credentials: 'same-origin',
      body: JSON.stringify({
        upscope_session: {
          lookup_code,
          agent_name: agentName,
          agent_email: agentEmail,
        }
      })
    }).then(response => response.json())
      .then((json) => {
        const watch_url = json.watch_url
        if (!watch_url) {
          error.textContent = json.message
          error.style.display = 'block'

          return
        }

        error.textContent = ''
        error.style.display = 'none'

        const win = window.open(watch_url, '_blank')
        win.focus()
      })
      .catch(console.error)
  }
})
