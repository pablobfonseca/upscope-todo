# frozen_string_literal: true

class UpscopeSessionService
  WATCH_URL = 'https://api.upscope.io/v1.1/visitors/:visitor_id/watch_url.json'
  VISITOR_URL = 'https://api.upscope.io/v1.1/list.json'

  def initialize(params)
    @lookup_code = params[:lookup_code]
    @agent_name  = params[:agent_name]
    @agent_email = params[:agent_email]
  end

  def perform
    result = find_user

    return not_found unless result

    generate_watch_url(result.short_id)
  end

  def find_user
    response = HTTParty.get(
      VISITOR_URL + "?search=#{lookup_code}",
      headers: {
        'X-Api-Key' => Rails.application.credentials.dig(:upscope, :secret_api_key)
      }
    )

    return unless parsed_response(response)[:visitors].any?

    OpenStruct.new(short_id: parsed_response(response).dig(:visitors, 0, :short_id))
  end

  def generate_watch_url(short_id)
    response = HTTParty.post(
      WATCH_URL.gsub(':visitor_id', short_id),
      headers: {
        'X-Api-Key' => Rails.application.credentials.dig(:upscope, :secret_api_key),
        'Content-Type' => Mime[:json].to_s,
        'Accept' => Mime[:json].to_s
      },
      body: request_watch_url_body
    )

    OpenStruct.new(watch_url: parsed_response(response)[:watch_url])
  end

  private

  attr_reader :lookup_code, :agent_email, :agent_name

  def request_watch_url_body
    {
      branding: {
        retry_url: 'http://localhost:3000/cobrowse',
        on_end_url: 'http://localhost:3000'
      },
      agent: {
        id: agent_email,
        name: agent_name
      }
    }.to_json
  end

  def parsed_response(response)
    JSON.parse(response.body, symbolize_names: true)
  end

  def not_found
    OpenStruct.new(message: 'User Not Found')
  end
end
