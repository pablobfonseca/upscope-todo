Rails.application.routes.draw do
  root to: 'items#index'

  resources :items do
    patch :complete, on: :member
  end

  get '/cobrowse', to: 'cobrowse#new'
  post '/cobrowse', to: 'cobrowse#create'
end
