class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :description
      t.date :due_date
      t.boolean :completed, default: false

      t.timestamps
    end
  end
end
